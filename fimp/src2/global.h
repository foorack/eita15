#ifndef GLOBAL_H_
#define GLOBAL_H_

#define DEVICE_ID 0x02		// Device ID, 2 byte hex
#define UART1_MODE 2		// 1 for screen, 2 for terminal
#define F_CPU 16000000UL

extern int NUMBERS[10][10];
extern char HEX[16];

#endif /* GLOBAL_H_ */
