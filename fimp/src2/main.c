#include "global.h"
#include <avr/io.h>
#include <util/delay.h>

int main(void) {

	DDRD = 0b11100000;

	uint8_t state = 0;
	while(1){
		state = PINB & 0b00001100;
//		_delay_ms(500);
//		PORTD ^= 0b11100000;
		if(state != 0) {
			PORTD = 0b11100000;
		} else {
			PORTD = 0;
		}
	}
}

