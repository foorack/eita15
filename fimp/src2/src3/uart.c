#include "global.h"
#include "uart.h"

volatile uint8_t receivedData_PC;
volatile uint8_t receivedData_LoRa;
volatile uint8_t loraState = 0;

void init_uart0() {
    int UBRR_value = 16;

    UBRR0H = (unsigned char) (UBRR_value >> 8);
    UBRR0L = (unsigned char) UBRR_value;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0); // Enable both receiver and transmitter. Note that the TX- and RX-pins are now no longer IO-pins.
    UCSR0C |= (0 << USBS0) | (3 << UCSZ01); // Stop bits (two of them) and length (8 bit)
    UCSR0B |= (1 << RXCIE0) | (0 << TXCIE0); // Enable interrupts!
}

void init_uart1() {
    int UBRR_value = 16;

    UBRR1H = (unsigned char) (UBRR_value >> 8);
    UBRR1L = (unsigned char) UBRR_value;
    UCSR1B = (1 << RXEN1) | (1 << TXEN1); // Enable both receiver and transmitter. Note that the TX- and RX-pins are now no longer IO-pins.
    UCSR1C |= (1 << USBS1) | (3 << UCSZ10); // Stop bits (two of them) and length (8 bit)
    UCSR1B |= (1 << RXCIE1) | (0 << TXCIE1); // Enable interrupts!
}

void transmitUART0(uint8_t data) {
    //Wait until the Transmitter is ready
    while (! (UCSR0A & (1 << UDRE0)) );
    //Send data
    UDR0 = data;
}

void transmitUART0_string(char *aChar) {
    while (*aChar) {
        transmitUART0(*aChar++);
    }
}

void transmitUART1(uint8_t data) {
    //Wait until the Transmitter is ready
    while (! (UCSR1A & (1 << UDRE1)) );
    //Send data
    UDR1 = data;
}

void transmitUART1_string(char *aChar) {
    while (*aChar) {
        transmitUART1(*aChar++);
    }
}

ISR(USART0_RX_vect) {
    uint8_t receivedData_LoRa_tmp = UDR0;
    if(UART1_MODE == 2) {
        transmitUART1(receivedData_LoRa);
    }
    if(receivedData_LoRa == 'a' && receivedData_LoRa_tmp == 'c') {
        loraState = 1;
    } else if(receivedData_LoRa == 'd' && receivedData_LoRa_tmp == 'e') {
        loraState = 2;
    }
    receivedData_LoRa = receivedData_LoRa_tmp;
}

#if UART1_MODE == 2
ISR(USART1_RX_vect) {
    receivedData_PC = UDR1;
    transmitUART0(receivedData_PC);
    transmitUART1(receivedData_PC);
}
#endif
