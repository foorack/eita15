#include "global.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>
#include "goldelox.h"

void OpenComm() {
    _delay_ms(4000); // Will not accept commands until 3sec after startup

    int UBRR_value = 103; // 9600 Baud @ 16 MHz
    UBRR1H = (unsigned char) (UBRR_value >> 8);
    UBRR1L = (unsigned char) UBRR_value;
    UCSR1B = (1 << RXEN1) | (1 << TXEN1); // Enable both receiver and transmitter. Note that the TX- and RX-pins are now no longer IO-pins.
    UCSR1C |= (1 << USBS1) | (3 << UCSZ11); // Stop bits (two of them) and length (8 bit)

    setbaudWait(0x0019);
    _delay_ms(200);

    UBRR_value = 8; // 115200 Baud @ 16 MHz
    UBRR1H = (unsigned char) (UBRR_value >> 8);
    UBRR1L = (unsigned char) UBRR_value;
}

void txt_MoveCursor(WORD line, WORD column) {
    write_Data(0xFFE4);
    write_Data(line);
    write_Data(column);
    read_Ack();
}
void putCH(WORD character) {
    write_Data(0xFFFE);
    write_Data(character);
    read_Ack();
}
void putstr(char *string) {
    write_Data(0x0006);
    while (*string) {
        write_Uart(*string++);
    }
    write_Uart(0x0);
    read_Ack();
}
WORD charwidth(char character) {
    write_Data(0x0002);
    write_Uart(character);
    read_Ack();
    return read_Data();
}
WORD charheight(char character) {
    write_Data(0x0001);
    write_Uart(character);
    read_Ack();
    return read_Data();
}
void txt_FGcolour(WORD colour) {
    write_Data(0xFF7F);
    write_Data(colour);
    read_Ack();
}
void txt_BGcolour(WORD colour) {
    write_Data(0xFF7E);
    write_Data(colour);
    read_Ack();
}
void txt_FontId(WORD id) {
    write_Data(0xFF7D);
    write_Data(id);
    read_Ack();
}
void txt_Width(WORD multiplier) {
    write_Data(0xFF7C);
    write_Data(multiplier);
    read_Ack();
}
void txt_Height(WORD multiplier) {
    write_Data(0xFF7B);
    write_Data(multiplier);
    read_Ack();
}
void txt_Xgap(WORD pixelcount) {
    write_Data(0xFF7A);
    write_Data(pixelcount);
    read_Ack();
}
void txt_Bold(WORD mode) {
    write_Data(0xFF76);
    write_Data(mode);
    read_Ack();
}
void txt_Inverse(WORD mode) {
    write_Data(0xFF74);
    write_Data(mode);
    read_Ack();
}
void txt_Italic(WORD mode) {
    write_Data(0xFF75);
    write_Data(mode);
    read_Ack();
}
void txt_Opacity(WORD mode) {
    write_Data(0xFF77);
    write_Data(mode);
    read_Ack();
}
void txt_Underline(WORD mode) {
    write_Data(0xFF73);
    write_Data(mode);
    read_Ack();
}
void txt_Attributes(WORD value) {
    write_Data(0xFF72);
    write_Data(value);
    read_Ack();
}
void txt_Set(WORD function, WORD value) {
    write_Data(0xFFE3);
    write_Data(function);
    write_Data(value);
    read_Ack();
}

void gfx_Cls() {
    write_Data(0xFFD7);
    read_Ack();
}
void gfx_ChangeColour(WORD oldColour, WORD newColour) {
    write_Data(0xFFBE);
    write_Data(oldColour);
    write_Data(newColour);
    read_Ack();
}
void gfx_Circle(WORD x, WORD y, WORD rad, WORD colour) {
    write_Data(0xFFCD);
    write_Data(x);
    write_Data(y);
    write_Data(rad);
    write_Data(colour);
    read_Ack();
}
void gfx_CircleFilled(WORD x, WORD y, WORD rad, WORD colour) {
    write_Data(0xFFCC);
    write_Data(x);
    write_Data(y);
    write_Data(rad);
    write_Data(colour);
    read_Ack();
}
void gfx_Line(WORD x1, WORD y1, WORD x2, WORD y2, WORD colour) {
    write_Data(0xFFD2);
    write_Data(x1);
    write_Data(y1);
    write_Data(x2);
    write_Data(y2);
    write_Data(colour);
    read_Ack();
}
void gfx_Rectangle(WORD x1, WORD y1, WORD x2, WORD y2, WORD colour) {
    write_Data(0xFFCF);
    write_Data(x1);
    write_Data(y1);
    write_Data(x2);
    write_Data(y2);
    write_Data(colour);
    read_Ack();
}
void gfx_RectangleFilled(WORD x1, WORD y1, WORD x2, WORD y2, WORD colour) {
    write_Data(0xFFCE);
    write_Data(x1);
    write_Data(y1);
    write_Data(x2);
    write_Data(y2);
    write_Data(colour);
    read_Ack();
}
void gfx_Polyline(WORD n, WORD vx[], WORD vy[], WORD colour) {
    write_Data(0x0005);
    for(int i = 0; i < n; i++) {
        write_Data(vx[i]);
    }
    for(int i = 0; i < n; i++) {
        write_Data(vy[i]);
    }
    write_Data(colour);
    read_Ack();
}
void gfx_Polygon(WORD n, WORD vx[], WORD vy[], WORD colour) {
    write_Data(0x0004);
    for(int i = 0; i < n; i++) {
        write_Data(vx[i]);
    }
    for(int i = 0; i < n; i++) {
        write_Data(vy[i]);
    }
    write_Data(colour);
    read_Ack();
}
void gfx_Triangle(WORD x1, WORD y1, WORD x2, WORD y2, WORD x3, WORD y3, WORD colour) {
    write_Data(0xFFC9);
    write_Data(x1);
    write_Data(y1);
    write_Data(x2);
    write_Data(y2);
    write_Data(x3);
    write_Data(y3);
    write_Data(colour);
    read_Ack();
}
void gfx_Orbit(WORD angle, WORD distance) {
    write_Data(0x0003);
    write_Data(angle);
    write_Data(distance);
    read_Ack();
}
void gfx_PutPixel(WORD x, WORD y, WORD colour) {
    write_Data(0xFFCB);
    write_Data(x);
    write_Data(y);
    write_Data(colour);
    read_Ack();
}
WORD gfx_GetPixel(WORD x, WORD y) {
    write_Data(0xFFCA);
    write_Data(x);
    write_Data(y);
    read_Ack();
    return read_Data();
}
void gfx_MoveTo(WORD x, WORD y) {
    write_Data(0xFFD6);
    write_Data(x);
    write_Data(y);
    read_Ack();
}
void gfx_LineTo(WORD x, WORD y) {
    write_Data(0xFFD4);
    write_Data(x);
    write_Data(y);
    read_Ack();
}
void gfx_Clipping(WORD value) {
    write_Data(0xFF6C);
    write_Data(value);
    read_Ack();
}
void gfx_ClipWindow(WORD x1, WORD y1, WORD x2, WORD y2) {
    write_Data(0xFFBF);
    write_Data(x1);
    write_Data(y1);
    write_Data(x2);
    write_Data(y2);
    read_Ack();
}
void gfx_SetClipRegion() {
    write_Data(0xFFBC);
    read_Ack();
}
void gfx_BGcolour(WORD colour) {
    write_Data(0xFF63);
    write_Data(colour);
    read_Ack();
}
void gfx_OutlineColour(WORD colour) {
    write_Data(0xFF67);
    write_Data(colour);
    read_Ack();
}
void gfx_Contrast(WORD contrast) {
    write_Data(0xFF66);
    write_Data(contrast);
    read_Ack();
}
void gfx_FrameDelay(WORD Msec) {
    write_Data(0xFF69);
    write_Data(Msec);
    read_Ack();
}
void gfx_LinePattern(WORD pattern) {
    write_Data(0xFF65);
    write_Data(pattern);
    read_Ack();
}
void gfx_ScreenMode(WORD mode) {
    write_Data(0xFF68);
    write_Data(mode);
    read_Ack();
}
void gfx_Set(WORD function, WORD value) {
    write_Data(0xFFD8);
    write_Data(function);
    write_Data(value);
    read_Ack();
}

WORD media_Init() {
    write_Data(0xFFB1);
    read_Ack();
    return read_Data();
}
void media_SetAdd(WORD HLword, WORD LOword) {
    write_Data(0xFFB9);
    write_Data(HLword);
    write_Data(LOword);
    read_Ack();
}
void media_SetSector(WORD HLword, WORD LOword) {
    write_Data(0xFFB8);
    write_Data(HLword);
    write_Data(LOword);
    read_Ack();
}
WORD media_ReadByte() {
    write_Data(0xFFB7);
    read_Ack();
    return read_Data();
}
WORD media_ReadWord() {
    write_Data(0xFFB6);
    read_Ack();
    return read_Data();
}
void media_WriteByte(WORD value) {
    write_Data(0xFFB5);
    write_Data(value);
    read_Ack();
}
void media_WriteWord(WORD value) {
    write_Data(0xFFB4);
    write_Data(value);
    read_Ack();
}
WORD media_Flush() {
    write_Data(0xFFB2);
    read_Ack();
    return read_Data();
}
void media_Image(WORD x, WORD y) {
    write_Data(0xFFB3);
    write_Data(x);
    write_Data(y);
    read_Ack();
}
void media_Video(WORD x, WORD y) {
    write_Data(0xFFBB);
    write_Data(x);
    write_Data(y);
    read_Ack();
}
void media_VideoFrame(WORD x, WORD y, WORD frameNumber) {
    write_Data(0xFFBA);
    write_Data(x);
    write_Data(y);
    read_Ack();
}

WORD peekB(WORD byteRegister) {
    write_Data(0xFFF6);
    write_Data(byteRegister);
    read_Ack();
    return read_Data();
}
void pokeB(WORD byteRegister, WORD value) {
    write_Data(0xFFF4);
    write_Data(byteRegister);
    write_Data(value);
    read_Ack();
}
WORD peekW(WORD byteRegister) {
    write_Data(0xFFF5);
    write_Data(byteRegister);
    read_Ack();
    return read_Data();
}
void pokeW(WORD byteRegister, WORD value) {
    write_Data(0xFFF3);
    write_Data(byteRegister);
    write_Data(value);
    read_Ack();
}

WORD joystick() {
    write_Data(0xFFD9);
    read_Ack();
    return read_Data();
}

void BeeP(WORD note, WORD duration) {
    write_Data(0xFFDA);
    write_Data(note);
    write_Data(duration);
    read_Ack();
}

void setbaudWait(WORD index) {
    write_Data(0x000B);
    write_Data(index);
    read_Ack();
}

void blitComtoDisplay(WORD x, WORD y, WORD width, WORD height, uint8_t data[]) {
    write_Data(0x000A);
    write_Data(x);
    write_Data(y);
    write_Data(width);
    write_Data(height);
    for(int i = 0; i < (width * height); i++) {
        write_Uart(data[i]);
    }
    read_Ack();
}

/**
 * Returns string with zero-suffixed.
 */
char* sys_GetModel() {
    write_Data(0x0007);
    read_Ack();
    WORD count = read_Data();
    char* str = malloc((count + 1)*sizeof(char));
    for(WORD i = 0; i != count; i++) {
        str[i] = read_Uart();
    }
    str[count] = 0x0;
    return str;
}
WORD sys_GetVersion() {
    write_Data(0x0008);
    read_Ack();
    return read_Data();
}
WORD sys_GetPmmC() {
    write_Data(0x0009);
    read_Ack();
    return read_Data();
}
void SSTimeout(WORD timeout) {
    write_Data(0x000C);
    write_Data(timeout);
    read_Ack();
}
void SSSpeed(WORD speed) {
    write_Data(0x000D);
    write_Data(speed);
    read_Ack();
}
void SSMode(WORD mode) {
    write_Data(0x000E);
    write_Data(mode);
    read_Ack();
}




void write_Data(WORD data) {
    write_Uart((uint8_t) (data >> 8));
    write_Uart((uint8_t) data);
}

/**
 * Writes a single byte to the display.
 *
 * @param data byte to write
 */
void write_Uart(uint8_t data) {
    // Wait for empty transmit buffer
    while (! (UCSR1A & (1 << UDRE1)) );
    // Put data into buffer, sends the data
    UDR1 = data;
}

WORD read_Data() {
    return (read_Uart() << 8) | read_Uart();
}

/**
 * Reads a single byte from the display.
 *
 * @return data byte read
 */
uint8_t read_Uart() {
    // Wait for data to be received
    while (! (UCSR1A & (1 << RXC1)) );
    // Get and return received data from buffer
    return UDR1;
}

void read_Ack() {
    uint8_t r = read_Uart();
    //if(r != 0x06){
    //    // do nothing for now
    //}
}
