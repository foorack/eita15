#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void init_uart0();
void init_uart1();
void transmitUART0(uint8_t data);
void transmitUART0_string(char *string);
void transmitUART1(uint8_t data);
void transmitUART1_string(char *string);
extern volatile uint8_t receivedData_LoRa;
extern volatile uint8_t receivedData_PC;
extern volatile uint8_t loraState;

#endif /* UART_H_ */
