#include "global.h"
#include "uart.h"
#include "goldelox.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define START_TIMER1 	TCCR1B |= (1 << CS12)|(0 << CS11)|(0 << CS10)

void init_pcint();
void init_timer1();
void printLTH();
void printNumber(int digit, int x, int y, int size, WORD colour);
void printDigit(int digit, int x, int y, int size, WORD colour);

int main(void) {

    DDRA = (1 << DDA1) | (1 << DDA0);
    DDRB = 0x00;
    DDRC = 0x00;
    DDRD = 0b11100000;

    // Enable LoRa commuication
    init_uart0();

    //init_pcint();
    //init_timer1();

#if UART1_MODE == 2
    // Enable Terminal communication
    init_uart1();
#elif UART1_MODE == 1
    // Enable uOLED screen communication
    OpenComm();
#endif

    // enable interrupts // cli(); // disable interrupts
    sei();

    // fancy startup screen
#if UART1_MODE == 1
    gfx_Cls();
    printLTH();
    _delay_ms(1000);
    gfx_Cls();
    putstr("Ansluter LoRa...");
#endif

    _delay_ms(2000);
    transmitUART0_string("mac set deveui 0004A30B0020D807\r\n");
    _delay_ms(100);
    transmitUART0_string("mac set appkey 8FA4907A688958D5CFAADBF6A5F33376\r\n");
    _delay_ms(100);
    transmitUART0_string("mac set appeui 70B3D57ED0012DA0\r\n");
    _delay_ms(100);
    transmitUART0_string("mac join otaa\r\n");

    int attempt = 0;
    for(uint8_t i = 0; loraState == 0 && i < 100; i++) {
        _delay_ms(100);
    }
#if UART1_MODE == 1
    if(loraState == 2) {
        gfx_RectangleFilled(0, 0, 100, 128, GREEN);
    } else {
        gfx_RectangleFilled(0, 0, 100, 128, RED);
    }
    _delay_ms(3000);
#elif UART1_MODE == 2
    if(loraState == 2) {
        transmitUART1_string("LORA SUCCESS\r\n");
    } else {
        transmitUART1_string("LORA FAIL\r\n");
    }
#endif

    uint8_t state = 0;
    uint8_t count = 0;
    uint8_t newcount = -1;
    unsigned long time = 0;

    while (1) {

//        if(time > 28800000UL) { // 28800000
//            char sendstr[] = "mac tx cnf 1 FFFF0000\r\n";
//            sendstr[13] = HEX[((DEVICE_ID >> 12) & 0x0F)];
//            sendstr[14] = HEX[((DEVICE_ID >> 8) & 0x0F)];
//            sendstr[15] = HEX[((DEVICE_ID >> 4) & 0x0F)];
//            sendstr[16] = HEX[((DEVICE_ID >> 0) & 0x0F)];
//
//            sendstr[17] = HEX[((count >> 12) & 0x0F)];
//            sendstr[18] = HEX[((count >>  8) & 0x0F)];
//            sendstr[19] = HEX[((count >>  4) & 0x0F)];
//            sendstr[10] = HEX[((count >>  0) & 0x0F)];
//            transmitUART0_string(sendstr);
//            time = 0;
//        }

        if(newcount != count) {
            newcount = count;
#if UART1_MODE == 1
            gfx_Cls();
            gfx_RectangleFilled(0, 0, 128, 25, NAVY);
            gfx_RectangleFilled(30, 5, 32, 20, WHITE); // I

            gfx_RectangleFilled(35, 5, 37, 20, WHITE); // D
            gfx_RectangleFilled(37, 5, 39, 7, WHITE);
            gfx_RectangleFilled(37, 18, 39, 20, WHITE);
            gfx_RectangleFilled(39, 7, 41, 9, WHITE);
            gfx_RectangleFilled(39, 16, 41, 18, WHITE);
            gfx_RectangleFilled(41, 9, 43, 16, WHITE);

            gfx_RectangleFilled(46, 7, 48, 20, WHITE); // A
            gfx_RectangleFilled(52, 7, 54, 20, WHITE);
            gfx_RectangleFilled(48, 5, 52, 8, WHITE);
            gfx_RectangleFilled(48, 13, 52, 15, WHITE);

            gfx_RectangleFilled(57, 7, 59, 18, WHITE); // G
            gfx_RectangleFilled(65, 13, 67, 18, WHITE);
            gfx_RectangleFilled(59, 5, 65, 8, WHITE);
            gfx_RectangleFilled(59, 13, 65, 15, WHITE);
            gfx_RectangleFilled(59, 18, 65, 20, WHITE);

            printNumber(newcount, 5, 50, 5, GOLD);
#endif
        }
        if(time % 2000 == 0) {
#if UART1_MODE == 1
            printNumber(newcount, 5, 50, 5, GOLD);
#endif
        }

        _delay_ms(10);
        time += 10;

#if UART1_MODE == 1
        if((PINB & PB1) != 0) {
            gfx_RectangleFilled(110, 0, 115, 5, YELLOW);
        }
        if((PINB & PB2) != 0) {
            gfx_RectangleFilled(115, 0, 120, 5, GREEN);
        }
#endif

        if((PINB & 0b00000100) != 0 || (PINB & 0b00001000) != 0) {
            if(state == 0) {
                count++;

            char sendstr[] = "mac tx cnf 1 FFFF0000\r\n";
            sendstr[13] = HEX[((DEVICE_ID >> 12) & 0x0F)];
            sendstr[14] = HEX[((DEVICE_ID >> 8) & 0x0F)];
            sendstr[15] = HEX[((DEVICE_ID >> 4) & 0x0F)];
            sendstr[16] = HEX[((DEVICE_ID >> 0) & 0x0F)];

            sendstr[17] = HEX[((count >> 12) & 0x0F)];
            sendstr[18] = HEX[((count >>  8) & 0x0F)];
            sendstr[19] = HEX[((count >>  4) & 0x0F)];
            sendstr[20] = HEX[((count >>  0) & 0x0F)];
            transmitUART0_string(sendstr);

#if UART1_MODE == 2
                transmitUART1_string("Fimp!\r\n");
#endif
            }
            state = 1;
	    PORTD = 0b11100000;
        } else {
            state = 0;
	    PORTD = 0;
        }

    }
}

void init_pcint() {

    PCMSK1 |= (1 << PCINT9) | (1 << PCINT8);
    PCICR |= (1 << PCIE1);

}

void init_timer1() {

    TIMSK1 = (1 << TOIE1);	// Enable timer1 overflow interrupt

}

ISR(PCINT1_vect) {
    gfx_RectangleFilled(50, 50, 100, 100, RED);
    START_TIMER1;
}

ISR(TIMER1_OVF_vect) {
    gfx_RectangleFilled(50, 50, 100, 100, GREEN);
}

#if UART1_MODE == 1
void printLTH() {
    // L
    gfx_RectangleFilled(0, 40, 10, 80, NAVY);
    gfx_RectangleFilled(0, 80, 30, 90, NAVY);

    // T
    gfx_RectangleFilled(30, 40, 80, 50, NAVY);
    gfx_RectangleFilled(50, 50, 60, 90, NAVY);

    // H
    gfx_RectangleFilled(90, 40, 100, 90, NAVY);
    gfx_RectangleFilled(100, 60, 110, 70, NAVY);
    gfx_RectangleFilled(110, 40, 120, 90, NAVY);
}

void printNumber(int digit, int x, int y, int size, WORD colour) {
    int i = 0;
    if(digit / 100 > 0) {
        printDigit(((digit - (digit % 100)) / 100), x + (i++ * 9 * size), y, size, colour);
        digit -= (digit - (digit % 100));
    }
    if(digit / 10 > 0) {
        printDigit(((digit - (digit % 10)) / 10), x + (i++ * 9 * size), y, size, colour);
        digit -= (digit - (digit % 10));
    }
    printDigit(digit, x + (i++ * 9 * size), y, size, colour);
}

void printDigit(int digit, int x, int y, int size, WORD colour) {
    int dx;
    int dy;
    for(int yi = 0; yi != 10; yi++) {
        for(int xi = 0; xi != 8; xi++) {
            if((NUMBERS[digit][yi] & (0x80 >> xi)) > 0) {
                dx = (xi * size) + x;
                dy = (yi * size) + y;
                gfx_RectangleFilled(dx, dy, dx + size, dy + size, colour);
            }
        }
    }
}
#endif /* UART1_MODE == 1 */
