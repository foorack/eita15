package com.foorack.loraapi.response;

import spark.*;
import java.util.*;
import lombok.*;
import org.json.*;

@RequiredArgsConstructor
public class DataResponse {

    private final ResponseStatus status;
    private final Map<String, Object> data;
    private final String message;

    @Override
    public String toString() {
        JSONObject obj = new JSONObject();

        obj.put("status", status.toString());
        if(status.isData()) obj.put("data", data);
        if(status.isMessage()) obj.put("message", message);

        return obj.toString();
    }

}