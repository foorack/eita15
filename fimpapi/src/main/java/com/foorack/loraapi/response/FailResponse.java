package com.foorack.loraapi.response;

import java.util.*;

public class FailResponse extends DataResponse {

    public FailResponse(Map<String, Object> data) {
        super(ResponseStatus.FAIL, data, null);
    }

}