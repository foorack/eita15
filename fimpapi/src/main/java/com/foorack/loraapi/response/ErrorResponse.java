package com.foorack.loraapi.response;

import java.io.*;

public class ErrorResponse extends DataResponse {

    public static String exceptionToString(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        e.printStackTrace();
        return sw.toString();
    }

    public ErrorResponse(String message) {
        super(ResponseStatus.ERROR, null, message);
    }

    public ErrorResponse(Exception ex) {
        super(ResponseStatus.ERROR, null, exceptionToString(ex));
    }

}