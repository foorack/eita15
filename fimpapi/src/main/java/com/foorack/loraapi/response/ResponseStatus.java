package com.foorack.loraapi.response;

import lombok.*;

// http://labs.omniti.com/labs/jsend

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum ResponseStatus {
    SUCCESS(true, false), // All went well, and (usually) some data was returned.
    FAIL(true, false),    // There was a problem with the data submitted, or some pre-condition of the API call wasn't satisfied
    ERROR(false, true);    // An error occurred in processing the request, i.e. an exception was thrown

    @Getter private final boolean data;
    @Getter private final boolean message;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}