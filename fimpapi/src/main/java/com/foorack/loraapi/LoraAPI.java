package com.foorack.loraapi;

import java.util.*;
import java.util.stream.*;
import java.io.*;
import lombok.*;
import com.foorack.loraapi.response.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import static spark.Spark.*;

public class LoraAPI {

    public static Device addDevice(String name, Location loc) {
        int id = listDevices(true).size();
        long time = System.currentTimeMillis();
        EventStorageAPI.getStorage().addEntry(new EventEntry("fimp", "device", time, Map.of("id", id + "", "name", name, "lat", loc.getLatitude() + "", "long", loc.getLongitude() + "", "action", "add")));
        return new Device(id, name, time, loc);
    }

    public static boolean removeDevice(int id) {
        Set<Integer> ids = listDevices(false);
        if(ids.contains(id)) {
            EventStorageAPI.getStorage().addEntry(new EventEntry("fimp", "device", Map.of("id", id + "", "action", "remove")));
            return true;
        }
        return false;
    }

    public static Set<Device> getDevices() {
        Set<Device> devs = new HashSet<>();
        EventStorageAPI.getStorage().getEntryStream("fimp", Arrays.asList("device")).sorted().forEach( ee -> {
            String action = ee.getData().get("action");
            int id = Integer.parseInt(ee.getData().get("id"));
            if(action.equals("add") || action.equals("update")) {
                devs.add(new Device(id,
                                    ee.getData().get("name"),
                                    ee.getTime(),
                                    new Location(Double.parseDouble(ee.getData().get("lat")),
                                                 Double.parseDouble(ee.getData().get("long")))
                                   ));
            } else {
                devs.remove(new Device(id, "", 0, new Location(0, 0))); // only id is important
            }
        });
        return devs;
    }

    public static Device getDevice(int id) {
        return EventStorageAPI.getStorage().getEntryStream("fimp", Arrays.asList("device"), Map.of("id", id + "")).sorted().map( ee -> {
            String action = ee.getData().get("action");
            if(action.equals("add") || action.equals("update")) {
                return new Device(Integer.parseInt(ee.getData().get("id")),
                                  ee.getData().get("name"),
                                  ee.getTime(),
                                  new Location(Double.parseDouble(ee.getData().get("lat")),
                                               Double.parseDouble(ee.getData().get("long")))
                                 );
            }
            return (Device) null;
        }).reduce((a, b) -> b).orElse(null);
    }

    public static Set<Integer> listDevices(boolean showDeleted) {
        Set<Integer> ids = new HashSet<>();
        EventStorageAPI.getStorage().getEntryStream("fimp", Arrays.asList("device")).sorted().forEach( ee -> {
            String action = ee.getData().get("action");
            int id = Integer.parseInt(ee.getData().get("id"));
            if(action.equals("remove") && !showDeleted) {
                ids.remove(id);
            } else if (action.equals("add") ||  action.equals("update")) {
                ids.add(id);
            }
        });
        return ids;
    }

    public static Report addReport(int id, int amount) {
        long time = System.currentTimeMillis();
        Device dev = getDevice(id);
        if(dev == null) {
            return null;
        }
        EventStorageAPI.getStorage().addEntry(new EventEntry("fimp", "report", time, Map.of("id", id + "", "amount", amount + "")));
        return new Report(id, amount, time);
    }

    public static List<Report> getReports() {
        Set<Integer> ids = listDevices(false);
        return EventStorageAPI.getStorage().getEntryStream("fimp", Arrays.asList("report")).sorted().filter(ee -> ids.contains(Integer.parseInt(ee.getData().get("id")))).map( ee -> {
            return new Report(Integer.parseInt(ee.getData().get("id")),
                              Integer.parseInt(ee.getData().get("amount")),
                              ee.getTime()
                             );
        }).collect(Collectors.toList());
    }

}