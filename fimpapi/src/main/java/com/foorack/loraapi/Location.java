package com.foorack.loraapi;

import lombok.*;

@Data @RequiredArgsConstructor
public class Location {

    private final double latitude, longitude;

}