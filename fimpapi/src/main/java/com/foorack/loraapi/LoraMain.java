package com.foorack.loraapi;

import java.util.*;
import java.util.function.*;
import java.io.*;
import java.nio.*;
import lombok.*;
import org.jline.reader.*;
import org.json.*;
import com.foorack.loraapi.response.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import static spark.Spark.*;

@lombok.extern.log4j.Log4j2
public class LoraMain {

    public static void main(String[] args) {
        new LoraMain();
    }
    private static final File TEST_DIR = new File("data");
    private static final File TEST_FILE = new File("data.yml");

    public LoraMain() {
        // Setup DEV ESAPI, move to mongo later
        TEST_DIR.mkdir();
        EventStorageAPI.registerInit(new StorageYAML(new EmptyEventStoragePlugin(), TEST_DIR));
        EventStorageAPI.getStorage().open();

        // Setup routes
        port(8337);
        ipAddress("127.0.0.1");
        options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }
            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            return "OK";
        });
        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "*");
            response.header("Access-Control-Allow-Headers", "*");
            response.type("application/json");
        });
        // Lora
        post("/", (req, res) -> {
            log.info("Authorization: " + req.headers("Authorization"));
            JSONObject datain = new JSONObject(req.body());
            log.info(datain.toString());
            log.info("downlink_url: " + datain.getString("downlink_url"));
            log.info("metadata: " + datain.getJSONObject("metadata").toString());
            log.info("hardware_serial: " + datain.getString("hardware_serial"));
            log.info("payload_raw: " + datain.getString("payload_raw"));
            byte[] data = Base64.getDecoder().decode(datain.getString("payload_raw"));
            String datahex = bytesToHex(data);
            log.info("payload: " + datahex);
            log.info("counter: " + datain.getInt("counter"));
            log.info("app_id: " + datain.getString("app_id"));
            log.info("dev_id: " + datain.getString("dev_id"));

            Report rep = LoraAPI.addReport(
            (int) ByteBuffer.wrap(new byte[]{data[0], data[1]}).getShort(),
            (int) ByteBuffer.wrap(new byte[]{data[2], data[3]}).getShort());
            if(rep == null) {
                log.warn("Missing device");
            } else {
                log.info("time: " + rep.getTime());
            }
            return new SuccessResponse(Map.of("ok", System.currentTimeMillis()));
        });
        // Web
        get("/ping", (req, res) -> new SuccessResponse(Map.of("pong", System.currentTimeMillis())));
        get("/v1/device/add/:name/:lat/:lon", (req, res) -> {
            String name = req.params("name").replaceAll("[^A-ZÅÄÖa-zåäö0-9 ]", "");
            if(name.length() > 22) {
                return new FailResponse(Map.of("name", "max-len-22"));
            }
            double lat, lon;
            try {
                lat = Double.parseDouble(req.params("lat"));
            } catch(NumberFormatException ex) {
                return new FailResponse(Map.of("lat", "invalid"));
            }
            try {
                lon = Double.parseDouble(req.params("lon"));
            } catch(NumberFormatException ex) {
                return new FailResponse(Map.of("lon", "invalid"));
            }
            Device dev;
            try {
                dev = LoraAPI.addDevice(name, new Location(lat, lon));
            } catch(Exception ex) {
                return new ErrorResponse(ex);
            }
            return new SuccessResponse(Map.of("id", dev.getId()));
        });
        get("/v1/device/get", (req, res) -> {
            Set<Device> devs;
            try {
                devs = LoraAPI.getDevices();
            } catch(Exception ex) {
                return new ErrorResponse(ex);
            }
            return new SuccessResponse(Map.of("devices", devs));
        });
        get("/v1/device/get/:id", (req, res) -> {
            int id;
            try {
                id = Integer.parseInt(req.params("id"));
            } catch(NumberFormatException ex) {
                return new FailResponse(Map.of("id", "invalid"));
            }
            Device dev;
            try {
                dev = LoraAPI.getDevice(id);
            } catch(Exception ex) {
                return new ErrorResponse(ex);
            }
            if(dev == null) {
                return new FailResponse(Map.of("id", "missing"));
            }
            return new SuccessResponse(Map.of("device", dev));
        });
        get("/v1/device/remove/:id", (req, res) -> {
            int id;
            try {
                id = Integer.parseInt(req.params("id"));
            } catch(NumberFormatException ex) {
                return new FailResponse(Map.of("id", "invalid"));
            }
            boolean b;
            try {
                b = LoraAPI.removeDevice(id);
            } catch(Exception ex) {
                return new ErrorResponse(ex);
            }
            return new SuccessResponse(Map.of("status", b));
        });
        get("/v1/report/get", (req, res) -> {
            List<Report> reps;
            try {
                reps = LoraAPI.getReports();
            } catch(Exception ex) {
                return new ErrorResponse(ex);
            }
            return new SuccessResponse(Map.of("reports", reps));
        });

        startTerminal();
    }
    @SneakyThrows
    public void startTerminal() {
        Map<String, Consumer<String[]>> commands = new HashMap<>();

        // report commands
        commands.put("list-reports", (args) -> {
            List<Report> reps = LoraAPI.getReports();
            reps.forEach(dev -> {
                log.info("id: " + dev.getId() + " \tamount: " + dev.getAmount() + " \ttime: " + dev.getTime());
            });
        });
        commands.put("add-report", (args) -> {
            if(args.length != 2) {
                log.warn("Invalid usage");
            }
            Report rep = LoraAPI.addReport(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
            if(rep == null) {
                log.warn("Missing device");
            } else {
                log.info("time: " + rep.getTime());
            }
        });
        // device commands
        commands.put("list-devices", (args) -> {
            Set<Device> devs = LoraAPI.getDevices();
            devs.forEach(dev -> {
                log.info("id: " + dev.getId() + " \tname: " + dev.getName() + " \tlat: " + dev.getLocation().getLatitude() + " \tlong: " + dev.getLocation().getLongitude() + " \tcreated: " + dev.getCreated());
            });
        });
        commands.put("add-device", (args) -> {
            if(args.length != 3) {
                log.warn("Invalid usage");
            }
            Device dev = LoraAPI.addDevice(args[0], new Location(Double.parseDouble(args[1]), Double.parseDouble(args[2])));
            log.info("id: " + dev.getId());
        });
        commands.put("remove-device", (args) -> {
            if(args.length != 1) {
                log.warn("Invalid usage");
            }
            int id = Integer.parseInt(args[0]);
            boolean s = LoraAPI.removeDevice(id);
            if(s) {
                log.info("id: " + id);
            } else {
                log.warn("Missing device");
            }
        });
        // exit & quit
        commands.put("help", (args) -> {
            commands.keySet().forEach(log::info);
        });
        commands.put("quit", (args) -> {
            EventStorageAPI.getStorage().close();
            System.exit(0);
        });
        commands.put("exit", (args) -> {
            EventStorageAPI.getStorage().close();
            System.exit(0);
        });

        LineReader reader = LineReaderBuilder.builder().build();
        String line = null;
        try {
            while((line = reader.readLine(" > ")) != null) {
                if(line.length() == 0) {
                    continue;
                }
                String[] parts = line.split(" ");
                Consumer trig = commands.get(parts[0]);
                String[] args = Arrays.copyOfRange(parts, 1, parts.length);
                if(trig != null) {
                    try {
                        trig.accept(args);
                    } catch(Exception e) {
                        log.error(e.getMessage());
                    }
                    continue;
                }
                log.warn("Invalid command");
            }
        } catch(org.jline.reader.UserInterruptException e) {
            commands.get("exit").accept(new String[0]);
        }
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
