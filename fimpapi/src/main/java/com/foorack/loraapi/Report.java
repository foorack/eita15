package com.foorack.loraapi;

import lombok.*;
import org.json.*;

@Data
public class Report {

    @NonNull private final int id;
    @NonNull private final int amount;
    @NonNull private final long time;

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Report)) {
            return false;
        }
        Report d2 = (Report) o;
        return d2.time == time;
    }

    @Override
    public int hashCode() {
        return (int) (time % Integer.MAX_VALUE);
    }

}