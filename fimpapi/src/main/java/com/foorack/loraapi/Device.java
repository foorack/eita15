package com.foorack.loraapi;

import lombok.*;
import org.json.*;

@Data
public class Device {

    @NonNull private final int id;
    @NonNull private final String name;
    @NonNull private final long created;
    @NonNull private final Location location;

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Device)) {
            return false;
        }
        Device d2 = (Device) o;
        return d2.id == id;
    }

    @Override
    public int hashCode() {
        return id;
    }

}