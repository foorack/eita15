const FUMI_API_URL = "https://fumi-api.foorack.com/v1/";
//const FUMI_API_URL = "http://localhost/v1/";

function removeDevice(id, callback) {
	_call_api("device/remove/" + id, {}, callback);
}
function addDevice(name, lat, lon, callback) {
	_call_api("device/add/" + name + "/" + lat + "/" + lon, {}, callback);
}
function getDevices(callback) {
	_call_api("device/get", {}, callback);
}
function getReports(callback) {
	_call_api("report/get", {}, callback);
}
function _call_api(url, data, callback) {
	if(callback === undefined){
		callback = function nul() {};
	}
	$.getJSON(FUMI_API_URL + url, data)
	.done(function( json ) {
		console.log( "JSON Data:", json );
		if(json.status === "success") {
			callback(json.data);
		} else if(json.status === "fail") {
			console.log("Request Failed:", json.data);
			alert("Request Failed! The error was user-side. Please try correct any data you filled out and try again. If the error persists, please contact support. Tell them how to replicate and they should find the error in the console.");
		} else if(json.status === "error") {
			console.log("Request Error:", json.message);
			alert("Request Error! The error was server-side. Please contact support and ask them to look for errors in their logs.");
		}
	})
	.fail(function( jqxhr, textStatus, error ) {
		var err = textStatus + ", " + error;
		console.log("Request Failed: " + err);
		alert("Request Failed: " + err);
	});
}