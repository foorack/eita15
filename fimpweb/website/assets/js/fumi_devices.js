function deleteDeviceForm() {
	var id = document.getElementById("deviceDeleteID").innerHTML;
	removeDevice(id, function() {window.location.reload()});
}
window.addEventListener("load", function(){
	if ($('#page-uid-devices').length === 1) {
		getDevices(function (data) {
			data.devices.forEach(function callback(dev) {
				$('#devices-list').append('<tr><td>' + dev.id + '</td><td>' + dev.name + '</td><td>' + dev.location.latitude + '</td><td>' + dev.location.longitude + '</td><td>' + new Date(dev.created).toString().split(" ").slice(0,5).join(" ") + '</td><td><a class="btn btn-primary btn-sm" href="/device-edit/' + dev.id + '"><i class="fas fa-edit"></i></a> <a class="btn btn-danger btn-sm" href="#" data-toggle="modal" data-target="#deleteModal" onclick="document.getElementById(\'deviceDeleteID\').innerHTML = ' + dev.id + ';"><i class="fas fa-times"></i></a></td></tr>');
			});
	
			// Call the dataTables jQuery plugin
			$(document).ready(function() {
		        	var table = $('#dataTable').DataTable();
				var id = new URL(window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search).searchParams.get("id")
				if(id !== null) {
					table.columns(0).search(id).draw();
					$('<div class="alert alert-warning mb-0" role="alert">Varning, <b>endast</b> enheten med ID = ' + id + ' visas på denna sida.<br>Tryck på "Enheter" till vänster för att komma tillbaka till normal yv.</div>').insertAfter("#create-device-button");
				}
			});
		});
	}
});
