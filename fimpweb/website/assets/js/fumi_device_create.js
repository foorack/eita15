function registerDeviceForm(e) {
	e.preventDefault();
	var name = $('#inputName').val();
	var lat = $('#inputLatitude').val();
	var lon = $('#inputLongitude').val();

	addDevice(name, lat, lon, function () {window.history.back();});
}
if ($('#page-uid-device-create').length === 1) {
	$('#registerDeviceButton').click(registerDeviceForm);
}