function calcAmountAtTime(devices, reports, date) {
	if(date.getFullYear() < 2018){
		return 0;
	}
	let total = 0;
	for(let di = 0; di != devices.length; di++) {
		devices[di].count = 0;
		devices[di].counttemp = 0;
		devices[di].reports = [];
	}
	for(let ri = 0; ri != reports.length; ri++) {
		let rep = reports[ri];
		if(rep.time > date.getTime()){
			continue;
		}
		for(let di = 0; di != devices.length; di++) {
			let dev = devices[di];
			if(dev.id == rep.id) {
				if(dev.counttemp > rep.amount) {
					dev.counttemp = 0;
					dev.count += rep.amount - dev.counttemp;
				} else {
					dev.count += rep.amount - dev.counttemp;
					dev.counttemp = rep.amount;
				}
				dev.reports.push(rep);
			}
		}
	}
	for(let di = 0; di != devices.length; di++) {
		total += devices[di].count;
	}
	return total;
}

var first = false;

function runIndexData() {
		// Set new default font family and font color to mimic Bootstrap's default styling
		Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
		Chart.defaults.global.defaultFontColor = '#292b2c';

		getReports(function (repdata) {
			getDevices(function (devdata) {
				var totlat = 0;
				var totlon = 0;
				for(let di = 0; di != devdata.devices.length; di++) {
					let dev = devdata.devices[di];
					dev.count = 0;
					dev.counttemp = 0;
					dev.reports = [];
					totlat += dev.location.latitude;
					totlon += dev.location.longitude;
				}
				if (!first) {
					var map = L.map('mapid', {
						center: [totlat / devdata.devices.length, totlon / devdata.devices.length],
						zoom: 13
					});
					L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
						attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
					}).addTo(map);
	
					devdata.devices.forEach(dev => {
						L.marker([dev.location.latitude, dev.location.longitude]).addTo(map)
							.bindPopup('#' + dev.id + ' - ' + dev.name);
					});
					first = true;
				}
				// Update last updated timestamp
				$('.last-updated').html("Uppdaterad den " + new Date().toString().split(" ").slice(0, 5).join(" "));

				let total = 0;
				let todayc = 0;
				let warningfull = 0;
				const now = new Date();
				const midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
				for(let ri = 0; ri != repdata.reports.length; ri++) {
					let rep = repdata.reports[ri];
					if(rep.time > midnight){
						todayc++;
					}
					for(let di = 0; di != devdata.devices.length; di++) {
						let dev = devdata.devices[di];
						if(dev.id == rep.id) {
							if(dev.counttemp > rep.amount) {
								dev.counttemp = 0;
								dev.count += rep.amount - dev.counttemp;
							} else {
								dev.count += rep.amount - dev.counttemp;
								dev.counttemp = rep.amount;
							}
							dev.reports.push(rep);
						}
					}
				}
				for(let di = 0; di != devdata.devices.length; di++) {
					let dev = devdata.devices[di];
					total += dev.count;
					if(dev.count >= 30) {
						warningfull++;
					}
				}
				console.log(repdata);
				console.log(devdata);
				$('#index-colorcard-1').text(todayc);
				$('#index-colorcard-2').text(devdata.devices.length);
				$('#index-colorcard-3').text(total);
				$('#index-colorcard-4').text(warningfull);

				let timecodes1 = []
				let timedata1 = []
				let timecodes2 = []
				let timedata2 = []
				let timecodes3 = []
				let timedata3 = []

				let hour = now.getHours();
				let day = now.getDate();
				for(let dani = 0; dani != 13; dani++) {
					let ht = hour - 13 + dani + 1;
					let dt = day - 13 + dani + 1;
					if(ht < 0) ht += 24;
					if(ht > 23) ht -= 24;
					if(dt < 1) dt += 31;
					if(dt > 31) dt -= 31;
					timecodes1.push(ht + ":00");
					timedata1.push(calcAmountAtTime(devdata.devices, repdata.reports, new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour - 12 + dani, 59, 59)));

					timecodes2.push(dt);
					timedata2.push(calcAmountAtTime(devdata.devices, repdata.reports, new Date(now.getFullYear(), now.getMonth(), day - 12 + dani, 23, 59, 59)));

					timecodes3.push(ht + ":00");
					timedata3.push(calcAmountAtTime(devdata.devices, repdata.reports,
	new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour - 12 + dani, 59, 59)) - 
calcAmountAtTime(devdata.devices, repdata.reports, new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour - 13 + dani, 59, 59)));
				}

				var ctx = document.getElementById("myAreaChart1");
				var myLineChart = new Chart(ctx, {
				  type: 'line',
				  data: {
				    labels: timecodes1,
				    datasets: [{
				      label: "Fimpar / Timme - Totalt",
				      lineTension: 0.0,
				      backgroundColor: "rgba(2,117,216,0.2)",
				      borderColor: "rgba(2,117,216,1)",
				      pointRadius: 5,
				      pointBackgroundColor: "rgba(2,117,216,1)",
				      pointBorderColor: "rgba(255,255,255,0.8)",
				      pointHoverRadius: 5,
				      pointHoverBackgroundColor: "rgba(2,117,216,1)",
				      pointHitRadius: 50,
				      pointBorderWidth: 2,
				      data: timedata1,
				    }],
				  },
				  options: {
				    animation: false,
				    scales: {
				      xAxes: [{
				        time: {
				          unit: 'date'
				        },
				        gridLines: {
				          display: false
				        },
				        ticks: {
				          maxTicksLimit: 7
				        }
				      }],
				      yAxes: [{
				        ticks: {
				          min: 0,
				          max: total,
				          maxTicksLimit: 5
				        },
				        gridLines: {
				          color: "rgba(0, 0, 0, .125)",
				        }
				      }],
				    },
				    legend: {
				      display: true
				    }
				  }
				});

				var ctx = document.getElementById("myAreaChart2");
				var myLineChart = new Chart(ctx, {
				  type: 'line',
				  data: {
				    labels: timecodes2,
				    datasets: [{
				      label: "Fimpar / Dag - Totalt",
				      lineTension: 0.0,
				      backgroundColor: "rgba(2,117,216,0.2)",
				      borderColor: "rgba(2,117,216,1)",
				      pointRadius: 5,
				      pointBackgroundColor: "rgba(2,117,216,1)",
				      pointBorderColor: "rgba(255,255,255,0.8)",
				      pointHoverRadius: 5,
				      pointHoverBackgroundColor: "rgba(2,117,216,1)",
				      pointHitRadius: 50,
				      pointBorderWidth: 2,
				      data: timedata2,
				    }],
				  },
				  options: {
				    animation: false,
				    scales: {
				      xAxes: [{
				        time: {
				          unit: 'date'
				        },
				        gridLines: {
				          display: false
				        },
				        ticks: {
				          maxTicksLimit: 7
				        }
				      }],
				      yAxes: [{
				        ticks: {
				          min: 0,
				          max: total,
				          maxTicksLimit: 5
				        },
				        gridLines: {
				          color: "rgba(0, 0, 0, .125)",
				        }
				      }],
				    },
				    legend: {
				      display: true
				    }
				  }
				});

				var ctx = document.getElementById("myAreaChart3");
				var myLineChart = new Chart(ctx, {
				  type: 'line',
				  data: {
				    labels: timecodes3,
				    datasets: [{
				      label: "Fimpar / Timme",
				      lineTension: 0.0,
				      backgroundColor: "rgba(16,117,216,0.2)",
				      borderColor: "rgba(216,117,2,1)",
				      pointRadius: 5,
				      pointBackgroundColor: "rgba(216,117,2,1)",
				      pointBorderColor: "rgba(255,255,255,0.8)",
				      pointHoverRadius: 5,
				      pointHoverBackgroundColor: "rgba(216,117,2,1)",
				      pointHitRadius: 50,
				      pointBorderWidth: 2,
				      data: timedata3,
				    }],
				  },
				  options: {
				    animation: false,
				    scales: {
				      xAxes: [{
				        time: {
				          unit: 'date'
				        },
				        gridLines: {
				          display: false
				        },
				        ticks: {
				          maxTicksLimit: 7
				        }
				      }],
				      yAxes: [{
				        ticks: {
				          min: 0,
				          max: total,
				          maxTicksLimit: 5
				        },
				        gridLines: {
				          color: "rgba(0, 0, 0, .125)",
				        }
				      }],
				    },
				    legend: {
				      display: true
				    }
				  }
				});
			});
		});
}	

window.addEventListener("load", function(){
	if ($('#page-uid-index').length === 1) {
		setInterval(runIndexData, 2000);
		runIndexData();
	}
});