function loadResultsData() {
	getReports(function (data) {
		$('#reports-list').html('');
		data.reports.forEach(function callback(rep) {
			$('#reports-list').append('<tr><td>' + rep.id + '</td><td>' + rep.amount + '</td><td>' + new Date(rep.time).toString().split(" ").slice(0,5).join(" ") + '</td><td><a class="btn btn-primary btn-sm" href="/devices/?id=' + rep.id + '"><i class="fas fa-eye"></i></a></td></tr>');
		});

		// Call the dataTables jQuery plugin
		$(document).ready(function() {
			$('#dataTable').DataTable();
		});

		// Update last updated timestamp
		$('.last-updated').html("Uppdaterad den " + new Date().toString().split(" ").slice(0, 5).join(" "));
	});
}

window.addEventListener("load", function(){
	if ($('#page-uid-results').length === 1) {
		setInterval(loadResultsData, 2000);
		loadResultsData();
	}
});
