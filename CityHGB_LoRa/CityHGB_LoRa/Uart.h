/*
 * Uart.h
 *
 * Created: 2018-02-08 20:23:22
 *  Author: Christoffer
 */ 


#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void transmitUART0(uint8_t data);
void init_uart0();
void transmitUART0_string(char *string);
void transmitUART1(uint8_t data);
void init_uart1();
void transmitUART1_string(char *string);
extern volatile uint8_t receivedData_LoRa;
extern volatile uint8_t receivedData_PC;

#endif /* UART_H_ */