/*
 * Uart.c
 *
 * Created: 2018-02-08 20:12:55
 *  Author: Christoffer
 */ 


//#define _9600BAUD

#define _57600BAUD


#include "globalDefines.h"
#include "Uart.h"

volatile uint8_t dataCount1 = 0;
volatile uint8_t receivedData_PC;
volatile uint8_t dataCount = 0;
volatile uint8_t receivedData_LoRa;
	
void init_uart0(){
#if F_CPU == 8000000UL


#ifdef _9600BAUD
	int UBRR_value = 51;	// 9600 Baud @ 8 MHz
#endif

#endif

#if F_CPU == 16000000UL

#ifdef _115200BUAD
	int UBRR_value = 8;		// 115200 Baud @ 16 MHz
#endif

#ifdef _57600BAUD
int UBRR_value = 16;		// 115200 Baud @ 16 MHz
#endif

#ifdef _9600BAUD
	int UBRR_value = 103;	// 9600 Baud @ 16 MHz
#endif

#endif
	UBRR0H	= (unsigned char) (UBRR_value >> 8);
	UBRR0L	= (unsigned char) UBRR_value;
	UCSR0B	= (1 << RXEN0)	| (1 << TXEN0);						// Enable both receiver and transmitter. Note that the TX- and RX-pins are now no longer IO-pins.
	UCSR0C |= (1 << USBS0)	| (1 << UCSZ01) | (1 << UCSZ01);	// Stop bits (two of them) and length (8 bit)
	UCSR0B |= (1 << RXCIE0) | (0 << TXCIE0);					// Enable interrupts!
	
	UCSR0C |= (1 << USBS0);
}

void init_uart1(){
#if F_CPU == 8000000UL


	#ifdef _9600BAUD
	int UBRR_value = 51;	// 9600 Baud @ 8 MHz
	#endif

	#endif

	#if F_CPU == 16000000UL

	#ifdef _115200BUAD
	int UBRR_value = 8;		// 115200 Baud @ 16 MHz
	#endif
	
	#ifdef _57600BAUD
	int UBRR_value = 16;		// 115200 Baud @ 16 MHz
	#endif

	#ifdef _9600BAUD
	int UBRR_value = 103;	// 9600 Baud @ 16 MHz
	#endif

#endif
	UBRR1H	= (unsigned char) (UBRR_value >> 8);
	UBRR1L	= (unsigned char) UBRR_value;
	UCSR1B	= (1 << RXEN1)	| (1 << TXEN1);						// Enable both receiver and transmitter. Note that the TX- and RX-pins are now no longer IO-pins.
	UCSR1C |= (1 << USBS1)	| (1 << UCSZ11) | (1 << UCSZ11);	// Stop bits (two of them) and length (8 bit)
	UCSR1B |= (1 << RXCIE1) | (0 << TXCIE1);					// Enable interrupts!
	
	UCSR1C |= (1 << USBS1);
}

void transmitUART1_string(char *aChar) {
	while (*aChar) {
		transmitUART1(*aChar++);
	}
	
}

void transmitUART1(uint8_t data) {
	//Wait until the Transmitter is ready
	while (! (UCSR1A & (1 << UDRE1)) );
	//Send data
	
	UDR1 = data;


}

void transmitUART0(uint8_t data) {
	//Wait until the Transmitter is ready
	while (! (UCSR0A & (1 << UDRE0)) );
	//Send data
	
	UDR0 = data;


}

void transmitUART0_string(char *aChar) {
	while (*aChar) {
		transmitUART0(*aChar++);
	}
	
}

ISR(USART0_RX_vect) {
	receivedData_LoRa = UDR0;
	transmitUART1(receivedData_LoRa);
	dataCount++;
}

ISR(USART1_RX_vect) {
	receivedData_PC = UDR1;
	transmitUART0(receivedData_PC);
	dataCount1++;
}